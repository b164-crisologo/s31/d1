const Task = require('../models/task');


//Controller Function for getting all the tasks


module.exports.getAllTask = () => {
	return Task.find({}).then( result => {
		return result;
	})
}


//Creating a Task
module.exports.createTask = (requestBody) => {
	//Create object
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

//Deleting a task
//"id" url parameter passed from the taskRoute.js

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

//Update a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}


//Activity GET
module.exports.getTaskById = (taskId) => {
	return Task.findById(taskId).then( result => {
		return result;
	})
}

//Activity PUT
module.exports.updateStatus = (statusId) => {
	return Task.findById(statusId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.status = "complete";

		return result.save().then((updatedStatus, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedStatus;
			}
		})
	})
}

