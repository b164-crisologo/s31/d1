const express = require('express');
const router = express.Router();


const TaskController = require('../controllers/taskController');
//Get all the tasks

router.get("/", (req, res) =>{
	TaskController.getAllTask().then(resultFromController => res.send(resultFromController));
})


//Creating a task
router.post("/", (req, res) => {
TaskController.createTask(req.body).then(result => res.send(result));
})



//delete a task
//URL "http://localhost:3001/tasks/:id"
//params parameter
router.delete("/:id", (req, res) => {
	TaskController.deleteTask(req.params.id).then(result => res.send (result));
})

//Update a task

router.put("/:id", (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})


//Activity Create a route for getting a specific task.
router.get("/:id", (req, res) => {
	TaskController.getTaskById(req.params.id, req.body).then(result => res.send(result))
})

//Activity  Create a route for changing the status of a task to "complete"
router.put("/:id/complete", (req, res) => {
	TaskController.updateStatus(req.params.id, req.body).then(result => res.send(result))
})




module.exports = router;